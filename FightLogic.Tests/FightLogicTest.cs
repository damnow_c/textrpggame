﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using GameLogic.Fight;
using DataAccess.Repos;
using Moq;
using NUnit.Framework;
using GameLogic;

namespace FightLogic.Tests
{
    [TestFixture]
    class FightTests
    {
        private Hero _hero;
        private Monster _monster;
        private Armor _armor;
        private Weapon _weapon;

        [SetUp]
        public void Init()
        {
            _hero = new Hero()
            {
                Id = 1,
                Name = "hero",
                CurrentHealth = 15,
                MaxHealth = 20,
                Agility = 3,
                Luck = 3,
                Power = 8,
                ArmorId = 1,
                WeaponId = 1,
                ItemsNames = "item",
                Points = 100,
            };
            _weapon = new Weapon
            {
                Id = 1,
                Name = "weapon",
                Points = 10,
            };
            _armor = new Armor
            {
                Id = 1,
                Name = "armor",
                Points = 20,
            };
            _monster = new Monster()
            {
                Id = 1,
                Name = "monster",
                MaxHealth = 20,
                Power = 10,
            };
        }

        [TearDown]
        public void Clean()
        {
            _hero = null;
            _monster = null;
            _armor = null;
            _weapon = null;
        }

        [Test]
        public void Return_string_when_hero_ran_away()
        {
            var heroRepo = new Mock<IHeroRepo>();
            var monsterRepo = new Mock<IMonsterRepo>();
            var weaponRepo = new Mock<IWeaponRepo>();

            weaponRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_weapon);
            monsterRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_monster);
            var fight = new FightsOperations(monsterRepo.Object, heroRepo.Object, weaponRepo.Object);
            
            Assert.IsNotNull(fight.RunAway(_hero, _monster.Id));
        }

        [Test]
        public void Return_int_when_hero_use_Hit()
        {
            var heroRepo = new Mock<IHeroRepo>();
            var monsterRepo = new Mock<IMonsterRepo>();
            var weaponRepo = new Mock<IWeaponRepo>();

            weaponRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_weapon);
            var fight = new FightsOperations(monsterRepo.Object, heroRepo.Object, weaponRepo.Object);

            Assert.IsNotNull(fight.Hit(_hero, _monster.Id, true));
        }

        [Test]
        public void Return_int_when_Monster_use_Hit()
        {
            var heroRepo = new Mock<IHeroRepo>();
            var monsterRepo = new Mock<IMonsterRepo>();
            var weaponRepo = new Mock<IWeaponRepo>();

            weaponRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_weapon);
            monsterRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_monster);
            var fight = new FightsOperations(monsterRepo.Object, heroRepo.Object, weaponRepo.Object);

            Assert.IsNotNull(fight.Hit(_hero, _monster.Id, false));
        }

        [Test]
        public void Return_int_as_heroes_armor_points()
        {
            var armorRepo = new Mock<IArmorRepo>();

            armorRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_armor);

            Assert.AreEqual(_armor.Points, armorRepo.Object.GetById(_hero.ArmorId).Points);
        }

        [Test]
        public void Return_int_as_heroes_weapon_points()
        {
            var weaponRepo = new Mock<IWeaponRepo>();

            weaponRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_weapon);

            Assert.AreEqual(_weapon.Points, weaponRepo.Object.GetById(_hero.WeaponId).Points);
        }

        [Test]
        public void Return_empty_list_of_SingleHit_when_hero_fight_with_zero_health()
        {
            var heroRepo = new Mock<IHeroRepo>();
            var monsterRepo = new Mock<IMonsterRepo>();
            var armorRepo = new Mock<IArmorRepo>();

            armorRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_armor);
            monsterRepo.Setup(x => x.GetById(It.IsAny<int>())).Returns(_monster);

            _hero.CurrentHealth = 0;

            var fight = new FightsOperations(monsterRepo.Object, heroRepo.Object, armorRepo.Object);

            Assert.AreEqual(fight.Fight(_hero, _monster.Id).HistoryOfAllHits, new List<SingleHit>());
        }
    }
}
