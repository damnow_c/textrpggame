﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class EquipmentOperations : IEquipmentOperations
    {
        private readonly IWeaponRepo _weaponRepo;
        private readonly IArmorRepo _armorRepo;
        private readonly IItemRepo _itemRepo;
        private readonly IHeroRepo _heroRepo;

        public EquipmentOperations(IWeaponRepo weaponRepo, IArmorRepo armorRepo, IItemRepo itemRepo, IHeroRepo heroRepo)
        {
            _weaponRepo = weaponRepo;
            _armorRepo = armorRepo;
            _itemRepo = itemRepo;
            _heroRepo = heroRepo;
        }

        public void AddWeapon(string name, int powerPoints)
        {
            Weapon newWeapon = new Weapon() { Name = name, Points = powerPoints };
            _weaponRepo.Create(newWeapon);
        }

        public void AddArmor(string name, int powerPoints)
        {
            Armor newArmor = new Armor() { Name = name, Points = powerPoints };
            _armorRepo.Create(newArmor);
        }

        public void AddItem(string name, int powerPoints, string typeOfModificator)
        {
            Item newItem = new Item() { Name = name, Points = powerPoints, TypeOfModificator = typeOfModificator };
            _itemRepo.Create(newItem);
        }

        public int GetItemsCount()
        {
            int itemsCount = 0;
            itemsCount = _itemRepo.GetAll().ToList().Count;

            return itemsCount;
        }

        public int GetWeaponsCount()
        {
            int weaponsCount = 0;
            weaponsCount = _weaponRepo.GetAll().ToList().Count;

            return weaponsCount;
        }

        public int GetArmorsCount()
        {
            int armorsCount = 0;
            armorsCount = _armorRepo.GetAll().ToList().Count;

            return armorsCount;

        }

        public int GetPowerOfArmor (int id)
        {
            return _armorRepo.GetById(id).Points;
        }

        public int GetPowerOfWeapon(int id)
        {
            return _weaponRepo.GetById(id).Points;
        }
    }
}