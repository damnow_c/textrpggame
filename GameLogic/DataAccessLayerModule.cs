﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Repos.Interfaces;
using DataAccess.Repos;

namespace GameLogic
{
    public class DataAccessLayerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IArmorRepo>().To<ArmorRepo>();
            Bind<IHeroRepo>().To<HeroRepo>();
            Bind<IIncidentRepo>().To<IncidentRepo>();
            Bind<IItemRepo>().To<ItemRepo>();
            Bind<IMonsterRepo>().To<MonsterRepo>();
            Bind<IWeaponRepo>().To<WeaponRepo>();
        }
    }
}