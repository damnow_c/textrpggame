﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameLogic.Fight
{
    public class SingleHit
    {
        public int Damage { get; set; }
        public string HistoryOfHit { get; set; }
    }
}
