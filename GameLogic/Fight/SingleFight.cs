﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameLogic.Fight
{
    public class SingleFight
    {
        public Boolean Win { get; set; }
        public List<SingleHit> HistoryOfAllHits { get; set; }

        public SingleFight()
        {
            HistoryOfAllHits = new List<SingleHit>();
        }
    }
}
