﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic.Fight
{
    public class FightsOperations : IFightsOperations
    {
        private readonly IMonsterRepo _monsterRepo;
        private readonly IHeroRepo _heroRepo;
        private readonly IWeaponRepo _weaponRepo;
        private readonly IArmorRepo _armorRepo;

        public FightsOperations(IMonsterRepo monsterRepo, IHeroRepo heroRepo)
        {
            _monsterRepo = monsterRepo;
            _heroRepo = heroRepo;
        }

        public FightsOperations(IMonsterRepo monsterRepo, IHeroRepo heroRepo, IArmorRepo armorRepo)
        {
            _monsterRepo = monsterRepo;
            _heroRepo = heroRepo;
            _armorRepo = armorRepo;
        }

        public FightsOperations(IMonsterRepo monsterRepo, IHeroRepo heroRepo, IWeaponRepo weaponRepo)
        {
            _monsterRepo = monsterRepo;
            _heroRepo = heroRepo;
            _weaponRepo = weaponRepo;
        }


        public FightsOperations(IMonsterRepo monsterRepo, IHeroRepo heroRepo, IWeaponRepo weaponRepo, IArmorRepo armorRepo)
        {
            _monsterRepo = monsterRepo;
            _heroRepo = heroRepo;
            _weaponRepo = weaponRepo;
            _armorRepo = armorRepo;
        }

        public int Hit(Hero hero, int monsterId, Boolean heroWasAttacking)
        {
            List<int> damage = new List<int> { 0, 0 };
            int singleHitDamage = 0;
            Monster monster = new Monster();
            int weaponPower = 0;
            Random random = new Random();
            monster = _monsterRepo.GetById(monsterId);
            weaponPower = _weaponRepo.GetById(hero.WeaponId).Points;

            if (heroWasAttacking)
            {
                singleHitDamage = random.Next(hero.Power - 1 + weaponPower, hero.Power * 2 + weaponPower + hero.Luck);
            }
            else
            {
                singleHitDamage = random.Next(monster.Power, monster.Power + 5);
            }

            return singleHitDamage;
        }

        public SingleFight Fight(Hero hero, int monsterId)
        {
            Monster monster = new Monster();
            SingleFight singleFight = new SingleFight();
            Boolean someonesDeath = false;
            int currentMonsterHealth = 0;
            monster = _monsterRepo.GetById(monsterId);
            int armorPoints = _armorRepo.GetById(hero.ArmorId).Points;

            SingleHit singleHit = new SingleHit();

            currentMonsterHealth = monster.MaxHealth;

            do
            {
                if (hero.CurrentHealth > 0 && currentMonsterHealth > 0)
                {
                    singleHit = new SingleHit();
                    singleHit.Damage = Hit(hero, monsterId, true);
                    currentMonsterHealth = currentMonsterHealth - singleHit.Damage;
                    if (currentMonsterHealth < 0)
                    {
                        currentMonsterHealth = 0;
                    }
                    singleHit.HistoryOfHit = $"Heros zadał potworowi {singleHit.Damage}, potwór posiada {currentMonsterHealth}/{monster.MaxHealth} punktów życia. (Wciśnij dowolny klawisz, by przejśc dalej)";
                    singleFight.HistoryOfAllHits.Add(singleHit);
                    if (currentMonsterHealth > 0)
                    {
                        singleHit = new SingleHit();
                        singleHit.Damage = Hit(hero, monsterId, false);
                        if (singleHit.Damage - armorPoints < 0)
                        {
                            singleHit.Damage = 0;
                        }
                        else
                        {
                            hero.CurrentHealth = hero.CurrentHealth + armorPoints - singleHit.Damage;
                        }
                        if (hero.CurrentHealth < 0)
                        {
                            hero.CurrentHealth = 0;
                        }
                        singleHit.HistoryOfHit = $"Potwór zadał Herosowi {singleHit.Damage - armorPoints} (pancerz zredukował: {armorPoints} obrażeń), Heros posiada {hero.CurrentHealth}/{hero.MaxHealth} punktów życia. (Wciśnij dowolny klawisz, by przejśc dalej)";
                        singleFight.HistoryOfAllHits.Add(singleHit);
                        _heroRepo.Update(hero);
                    }
                }
                else
                {
                    someonesDeath = true;
                }
            } while (!someonesDeath);

            if (hero.CurrentHealth > 0)
            {
                singleFight.Win = true;
                hero.Points = hero.Points + monster.MaxHealth + 2 * monster.Power;
                _heroRepo.Update(hero);
            }
            else
            {
                singleFight.Win = false;
            }

            return singleFight;
        }

        public string RunAway(Hero hero, int monsterId)
        {
            int damage = 0;
            string whatHappened = "";
            
            Monster monster = new Monster();
            monster = _monsterRepo.GetById(monsterId);

            if (new Random().Next(0, 100) + hero.Luck + hero.Agility * 2 > 60)
            {
                whatHappened = "Heros uciekł przed potworem.";
            }
            else
            {
                damage = Hit(hero, monsterId, false);
                hero.CurrentHealth -= damage;
                whatHappened = $"Potwór rani herosa próbującego uciec! Heros traci {damage} punktów zdrowia.";
                _heroRepo.Update(hero);
            }

            return whatHappened;
        }
    }
}
