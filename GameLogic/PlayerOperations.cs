﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class PlayerOperations : IPlayerOperations
    {
        private readonly IHeroRepo _heroRepo;
        private readonly IWeaponRepo _weaponRepo;
        private readonly IArmorRepo _armorRepo;

        public PlayerOperations(IHeroRepo heroRepo, IWeaponRepo weaponRepo, IArmorRepo armorRepo)
        {
            _heroRepo = heroRepo;
            _weaponRepo = weaponRepo;
            _armorRepo = armorRepo;
        }

        public Hero AddHero(string name, int maxHealth, int power, int luck, int agility)
        {
            Hero newHero = new Hero() { Name = name, MaxHealth = maxHealth, Power = power, Agility = agility, Luck = luck, CurrentHealth = maxHealth, WeaponId = 1, ArmorId = 1,  ItemsNames = "" };
            _heroRepo.Create(newHero);
            return newHero;
        }

        public Boolean AlreadyDied(Hero hero)
        {
            if (hero.CurrentHealth < 1)
            {
                return true;
            }
            return false;
        }

        public string HeroStatus(Hero hero)
        {
            string status = "";

            status = $"Heros: {hero.Name} Życie:{hero.CurrentHealth}/{hero.MaxHealth} Moc:{hero.Power}+{_weaponRepo.GetById(hero.WeaponId).Points} Punkty:{hero.Points}";

            return status;
        }

        public string HeroLastStatus(Hero hero)
        {
            string status = "";
            string heroesArmourName = "";
            string heroesWeaponName = "";
            heroesArmourName = _armorRepo.GetById(hero.ArmorId).Name;
            heroesWeaponName = _weaponRepo.GetById(hero.WeaponId).Name;
            if (hero.ItemsNames.Length < 2)
            {
                hero.ItemsNames += "Brak  ";
            }

            status = $"Heros: { hero.Name} Zginął dnia { hero.EndGameDate} Punkty: { hero.Points}\n" +
                $"Wyposażenie w trakcie śmierci:\n" +
                $"Pancerz: " + heroesArmourName +
                $"\nBroń: " + heroesWeaponName +
                $"\nPozostałe: " + hero.ItemsNames.Substring(0, hero.ItemsNames.Length - 2);

            return status;
        }

        public void SavePathToHistoryFile(Hero hero)
        {
            hero.EndGameDate = DateTime.Now;
            hero.PathToHistoryFile = $"{Directory.GetCurrentDirectory() + $"{hero.Id}{hero.Name}.txt"}";
            _heroRepo.Update(hero);
        }

        public string GetPathToHistoryFile(int heroId)
        {
            string pathToHistoryFile = "";
            Hero hero = _heroRepo.GetById(heroId);
            pathToHistoryFile = hero.PathToHistoryFile;
            return pathToHistoryFile;
        }

        public List<string> ChoosePlayerFromAlreadyDiedOrderedByPoints()
        {
            List<Hero> heros = new List<Hero>();
            List<String> ScoreBoard = new List<string>();
            heros = _heroRepo.GetAll().OrderByDescending(x => x.Points).ToList();

            foreach (Hero hero in heros)
            {
                ScoreBoard.Add(HeroStatus(hero));
            }
            return ScoreBoard;
        }

        public List<string> ChoosePlayerFromAlreadyDied()
        {
            List<Hero> heros = new List<Hero>();
            List<String> ScoreBoard = new List<string>();
            heros = _heroRepo.GetAll().OrderBy(x => x.Id).ToList();

            foreach (Hero hero in heros)
            {
                ScoreBoard.Add(HeroStatus(hero));
            }
            return ScoreBoard;
        }

        public void SaveNewLineToTextFileInCurrentLocalization(string text, Hero hero)
        {
            System.IO.File.AppendAllText($"{Directory.GetCurrentDirectory() + $"{hero.Id}{hero.Name}.txt"}", Environment.NewLine + $"{text}");
        }
    }
}
