﻿namespace GameLogic.Interfaces
{
    public interface IMonsterOperations
    {
        void AddMonster(string name, int maxHealth, int power);
        string MonsterStatus(int monsterId);
        int GetMonstersCount();
    }
}