﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace GameLogic.Interfaces
{
    public interface IPlayerOperations
    {
        Hero AddHero(string name, int maxHealth, int power, int luck, int agility);
        bool AlreadyDied(Hero hero);
        List<string> ChoosePlayerFromAlreadyDied();
        List<string> ChoosePlayerFromAlreadyDiedOrderedByPoints();
        string GetPathToHistoryFile(int heroId);
        string HeroLastStatus(Hero hero);
        string HeroStatus(Hero hero);
        void SavePathToHistoryFile(Hero hero);
        void SaveNewLineToTextFileInCurrentLocalization(string text, Hero hero);
    }
}