﻿using DataAccess.Entities;

namespace GameLogic.Interfaces
{
    public interface IIncidentsOperations
    {
        void AddIncident(string name, int healthChange, int kindOfEffect);
        string FoundArmor(Hero hero);
        string FoundItem(Hero hero);
        string FoundWeapon(Hero hero);
        string NeutralIncident(Hero hero, int incidentId);
        int GetIncidentsCount();
    }
}