﻿using DataAccess.Entities;

namespace GameLogic.Fight
{
    public interface IFightsOperations
    {
        SingleFight Fight(Hero hero, int monsterId);
        int Hit(Hero hero, int monsterId, bool heroWasAttacking);
        string RunAway(Hero hero, int monsterId);
    }
}