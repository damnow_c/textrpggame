﻿namespace GameLogic.Interfaces
{
    public interface IEquipmentOperations
    {
        void AddArmor(string name, int powerPoints);
        void AddItem(string name, int powerPoints, string typeOfModificator);
        void AddWeapon(string name, int powerPoints);
        int GetWeaponsCount();
        int GetArmorsCount();
        int GetItemsCount();
        int GetPowerOfArmor(int id);
        int GetPowerOfWeapon(int id);
    }
}