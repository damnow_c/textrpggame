﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class MonsterOperations : IMonsterOperations
    {
        private readonly IMonsterRepo _monsterRepo;

        public MonsterOperations(IMonsterRepo monsterRepo)
        {
            _monsterRepo = monsterRepo;
        }

        public void AddMonster(string name, int maxHealth, int power)
        {
            Monster newMonster = new Monster() { Name = name, MaxHealth = maxHealth, Power = power };
            _monsterRepo.Create(newMonster);
        }

        public string MonsterStatus(int monsterId)
        {
            Monster monster = new Monster();
            string status = "";
            monster = _monsterRepo.GetById(monsterId);

            status = $"Potwór: {monster.Name} (życie:{monster.MaxHealth} moc:{monster.Power})";

            return status;
        }

        public int GetMonstersCount()
        {
            int monstersCount = 0;
            if (_monsterRepo != null)
            {
                monstersCount = _monsterRepo.GetNumberOfMonsters();
            }
            return monstersCount;
        }
    }
}
