﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using GameLogic.Interfaces;

namespace GameLogic
{
    public class IncidentsOperations : IIncidentsOperations
    {
        private readonly IIncidentRepo _incidentRepo;
        private readonly IHeroRepo _heroRepo;
        private readonly IWeaponRepo _weaponRepo;
        private readonly IArmorRepo _armorRepo;
        private readonly IItemRepo _itemRepo;

        public IncidentsOperations(IIncidentRepo incidentRepo, IHeroRepo heroRepo, IItemRepo itemRepo, IWeaponRepo weaponRepo, IArmorRepo armorRepo)
        {
            _incidentRepo = incidentRepo;
            _heroRepo = heroRepo;
            _itemRepo = itemRepo;
            _weaponRepo = weaponRepo;
            _armorRepo = armorRepo;
        }

        public void AddIncident(string name, int healthChange, int kindOfEffect)
        {
            Incident newIncident = new Incident() { Name = name, Points = healthChange, KindOfEffect = kindOfEffect };
            _incidentRepo.Create(newIncident);
        }

        public string NeutralIncident(Hero hero, int incidentId)
        {
            Incident incident = new Incident();
            string whatHappened = "";
            incident = _incidentRepo.GetById(incidentId);

            switch (incident.KindOfEffect)
            {
                case 1:
                    if (hero.CurrentHealth + incident.Points > hero.MaxHealth)
                    {
                        hero.CurrentHealth = hero.MaxHealth;
                    }
                    else
                    {
                        hero.CurrentHealth += incident.Points;
                    }
                    whatHappened = $"Zdarzenie! {incident.Name} ({incident.Points})";
                    break;
                case 2:
                    hero.MaxHealth += incident.Points;
                    hero.CurrentHealth += incident.Points;
                    whatHappened = $"Zdarzenie! {incident.Name} ({incident.Points})";
                    break;
                case 3:
                    hero.Power += incident.Points;
                    whatHappened = $"Zdarzenie! {incident.Name} ({incident.Points})";
                    break;
                case 4:
                    whatHappened = FoundArmor(hero);
                    break;
                case 5:
                    whatHappened = FoundWeapon(hero);
                    break;
                case 6:
                    whatHappened = FoundItem(hero);
                    break;
                default:
                    break;
            }
            return whatHappened;
        }

        public string FoundArmor(Hero hero)
        {
            Boolean changed = false;
            int armorId = 0;
            string whatHappened = "";
            Armor heroesArmor = _armorRepo.GetById(hero.ArmorId);

            do
            {
                armorId = new Random().Next(1, _armorRepo.GetNumberOfArmors());
                Armor armor = _armorRepo.GetById(armorId);
                if (heroesArmor.Points < armor.Points)
                {
                    hero.ArmorId = armorId;
                    changed = true;
                    whatHappened = $"Heros znalazł {armor.Name}, jego obecny pancerz wynosi: {armor.Points}";
                }
            } while (!changed);

            _heroRepo.Update(hero);

            return whatHappened;
        }

        public string FoundWeapon(Hero hero)
        {
            Boolean changed = false;
            int weaponId = 0;
            string whatHappened = "";
            Weapon heroesWeapon = _weaponRepo.GetById(hero.WeaponId);

            do
            {
                weaponId = new Random().Next(1, _weaponRepo.GetNumberOfWeapons());
                Weapon weapon = _weaponRepo.GetById(weaponId);
                if (heroesWeapon.Points < weapon.Points)
                {
                    hero.WeaponId = weaponId;
                    changed = true;
                    whatHappened = $"Heros znalazł {weapon.Name}, dodatkowa moc pochodząca z broni wynosi: {weapon.Points}";
                }
            } while (!changed);

            _heroRepo.Update(hero);

            return whatHappened;
        }

        public string FoundItem(Hero hero)
        {
            string whatHappened = "";
            int itemId = 0;
            Boolean alreadyHas = false;
            Item item = new Item();

            do
            {
                itemId = new Random().Next(1, _itemRepo.GetNumberOfItems());
                item = _itemRepo.GetById(itemId);
                if (hero.ItemsNames.Contains(item.Name))
                {
                    alreadyHas = true;
                }
            } while (alreadyHas);

            hero.ItemsNames += $"{item.Name}, ";
            switch (item.TypeOfModificator)
            {
                case "health":
                    hero.MaxHealth += item.Points;
                    hero.CurrentHealth += item.Points;
                    whatHappened = $"Heros znalazł {item.Name}, jego maksymalne żcyie wynosi: {hero.MaxHealth}";
                    break;
                case "power":
                    hero.Power += item.Points;
                    whatHappened = $"Heros znalazł {item.Name}, jego obecna moc wynosi: {hero.Power}";
                    break;
                case "luck":
                    hero.Luck += item.Points;
                    whatHappened = $"Heros znalazł {item.Name}, jego obecne szczęście wynosi: {hero.Luck}";
                    break;
                default:
                    break;
            }

            _heroRepo.Update(hero);

            return whatHappened;
        }

        public int GetIncidentsCount()
        {
            int incidentsCount = 0;
            incidentsCount = _incidentRepo.GetAll().ToList().Count;
            return incidentsCount;
        }
    }
}