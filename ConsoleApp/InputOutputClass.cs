﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUserInterface
{
    class InputOutputClass
    {
        //example AskForData("Adres zakladki")
        public static string AskForData(string whatData)
        {
            Console.Write($"{whatData}: ");
            return (Console.ReadLine());
        }

        //example AskForValue("Wybor zakladki", 1, 110)
        public static int AskForIntFromTo(string whatData, int min, int max)
        {
            string data = "";
            int number = 0;
            Boolean success = false;
            do
            {
                Console.Write($"{whatData}: ");
                data = (Console.ReadLine());
                try
                {
                    number = Convert.ToInt32(data);
                    if (number >= min && number <= max)
                    {
                        success = true;
                    }
                }

                catch
                {
                    success = false;
                }

            } while (!success);

            return number;
        }

        public static int menuBar()
        {
            Console.Clear();
            int decyssion = 0;
            Console.WriteLine($"MENU\n1. Dodaj nowego bohatera.\n2. Dodaj nowego potwora.\n3. ZAGRAJ!.\n4. Wyświetl wyniki.\n5. Wyświetl historię wybranej gry.\n6. Zakończ.");
            decyssion = AskForIntFromTo("\nWybierz opcję", 1, 6);
            return decyssion;
        }
    }
}
