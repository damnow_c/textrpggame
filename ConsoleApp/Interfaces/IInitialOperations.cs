﻿namespace ConsoleApp.Interfaces
{
    public interface IInitialOperations
    {
        void AddMissinginitialData();
        void AddStartingArmor();
        void AddStartingItems();
        void AddStartingMonsters();
        void AddStartingWeapons();
        void InitialIncidents();
    }
}