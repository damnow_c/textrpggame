﻿using DataAccess.Entities;

namespace ConsoleApp.Interfaces
{
    public interface IMenuOptions
    {
        void CreateNewMonster();
        Hero CreateNewPlayer();
        void ShowGameHistory();
        void ShowScoreBoard();
        void StartNewGame(Hero hero);
    }
}