﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLogic;
using GameLogic.Interfaces;
using ConsoleApp.Interfaces;

namespace ConsoleApp
{
    public class InitialOperations : IInitialOperations
    {
        private readonly IEquipmentOperations _equipmentOperations;
        private readonly IIncidentsOperations _incidentsOperations;
        private readonly IMonsterOperations _monsterOperations;
        private readonly IPlayerOperations _playerOperations;

        public InitialOperations(IEquipmentOperations equipmentOperations, IIncidentsOperations incidentsOperations, IMonsterOperations monsterOperations, IPlayerOperations playerOperations)
        {
            _equipmentOperations = equipmentOperations;
            _incidentsOperations = incidentsOperations;
            _monsterOperations = monsterOperations;
            _playerOperations = playerOperations;
        }

        public void AddStartingMonsters()
        {
            for (int i = 0; i < 10; i++)
            {
                _monsterOperations.AddMonster($"Potwór{i + 1}", new Random().Next(10 + i * 5, 20 + i * 5), new Random().Next(i, i * 3));
            }
        }

        public void InitialIncidents()
        {
            _incidentsOperations.AddIncident("Znalazł miksturę uzdrowienia! Zyskuje zdrowie.", 20, 1);
            _incidentsOperations.AddIncident("Został pobłogosławiony przez pielgrzyma! Podwyższając maksymalną wartość jego życia!", 10, 2);
            _incidentsOperations.AddIncident("Został ukąszony przez węża! Traci zdrowie.", -20, 1);
            _incidentsOperations.AddIncident("Został popchnięty przez dziecko! Traci zdrowie.", -5, 1);
            _incidentsOperations.AddIncident("Ma pecha i został trafiony meteorytem! Traci zdrowie.", -70, 1);
            _incidentsOperations.AddIncident("Znalazł karnet na siłkę! Jego moc wzrasta.", 3, 3);
            _incidentsOperations.AddIncident("Znalazł wiadro sterydów! Jego moc wzrasta.", 5, 3);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 4);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 4);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 4);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 5);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 5);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 5);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 6);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 6);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 6);
            _incidentsOperations.AddIncident("Znalazł nowy przedmiot!", 0, 6);
            //1 health, 2 healthbuff, 3 powerbuff, 4 new equipment
        }

        public void AddStartingWeapons()
        {
            _equipmentOperations.AddWeapon("Pięści", 1);
            _equipmentOperations.AddWeapon("Topór", 5);
            _equipmentOperations.AddWeapon("Sztylet", 3);
            _equipmentOperations.AddWeapon("Patyk", 2);
            _equipmentOperations.AddWeapon("Miecz", 7);
        }

        public void AddStartingArmor()
        {
            _equipmentOperations.AddArmor("Podkoszulek", 1);
            _equipmentOperations.AddArmor("Chitynowy pancerz", 6);
            _equipmentOperations.AddArmor("Skórzany pancerz", 4);
            _equipmentOperations.AddArmor("Zbroja płytowa", 12);
            _equipmentOperations.AddArmor("Zdroja pierścieniowa", 8);
        }

        public void AddStartingItems()
        {
            _equipmentOperations.AddItem("Pierścień zdrowia", 10, "health");
            _equipmentOperations.AddItem("Pierścień mocy", 10, "power");
            _equipmentOperations.AddItem("Amulet zdrowia", 15, "health");
            _equipmentOperations.AddItem("Amulet mocy", 15, "power");
            _equipmentOperations.AddItem("Pierścień szczęścia", 10, "luck");
            _equipmentOperations.AddItem("Amulet szczęścia", 10, "luck");
            _equipmentOperations.AddItem("Totem nieszczęścia", -5, "luck");
        }

        public void AddMissinginitialData()
        {
            if (_monsterOperations.GetMonstersCount() < 10)
            {
                AddStartingMonsters();
            }

            if (_incidentsOperations.GetIncidentsCount() < 8)
            {
                InitialIncidents();
            }

            if (_equipmentOperations.GetArmorsCount() < 3)
            {
                AddStartingArmor();
            }

            if (_equipmentOperations.GetWeaponsCount() < 3)
            {
                AddStartingWeapons();
            }

            if (_equipmentOperations.GetItemsCount() < 3)
            {
                AddStartingItems();
            }
        }
    }
}
