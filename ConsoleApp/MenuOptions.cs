﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLogic;
using GameLogic.Interfaces;
using GameLogic.Fight;
using ConsoleApp.Interfaces;
using DataAccess.Entities;

namespace ConsoleUserInterface
{
    public class MenuOptions : IMenuOptions
    {
        private readonly IPlayerOperations _playerOperations;
        private readonly IMonsterOperations _monsterOperations;
        private readonly IIncidentsOperations _incidentsOperations;
        private readonly IFightsOperations _fightOperations;
        private readonly IEquipmentOperations _equipmentOperations;

        public MenuOptions(IPlayerOperations playerOperations, IMonsterOperations monsterOperations, IIncidentsOperations incidentsOperations, IFightsOperations fightOperations, IEquipmentOperations equipmentOperations)
        {
            _playerOperations = playerOperations;
            _monsterOperations = monsterOperations;
            _incidentsOperations = incidentsOperations;
            _fightOperations = fightOperations;
            _equipmentOperations = equipmentOperations;
        }

        public Hero CreateNewPlayer()
        {
            string name = "";
            int maxHealth = 0;
            int power = 0;
            int luck = 0;
            int agility = 0;

            Console.Clear();
            Console.WriteLine("Dodawanie nowego gracza.\n\n" +
                "Posiadasz 20 punktów statyskyk do rozdysponowania.\n" +
                "Każda wartość musi wynosić przynajmniej 1.\n\n" +
                "Żywotność - na jej podstawie zostanie ustalowa wartość życia herosa.\n" +
                "Moc - na jej podstawie ustalana będzie siła ataków herosa.\n" +
                "Szczęście - jego wartość zwiększa szanse na pozytywne zdarzenia herosa.\n" +
                "Zwinność - jej wartość zwiększa szanse na ucieczkę z niechcianej walki.\n\n");

            name = InputOutputClass.AskForData("Imię bohatera");
            maxHealth = InputOutputClass.AskForIntFromTo($"Żywotność (max:{17})", 1, 17);
            power = InputOutputClass.AskForIntFromTo($"Moc (max:{18 - maxHealth})", 1, 18 - maxHealth);
            luck = InputOutputClass.AskForIntFromTo($"Szczęście (max:{19 - power - maxHealth})", 1, 19 - power - maxHealth);
            agility = 20 - maxHealth - power - luck;
            Console.WriteLine($"Zwinność (niewykorzystane punkty):{agility}");

            Hero hero = _playerOperations.AddHero(name, maxHealth * 10, power, luck, agility);

            Console.WriteLine("\nDodano nowego Herosa! Naciśnij dowolny klawisz by przejść do menu.");
            Console.ReadKey();
            return hero;
        }

        public void CreateNewMonster()
        {
            string name = "";
            int maxHealth = 0;
            int power = 0;

            Console.Clear();
            Console.WriteLine("Dodawanie nowego potwora.\n\n" +
                "Każda wartość musi wynosić przynajmniej 1.\n\n" +
                "Żywotność - na jej podstawie zostanie ustalowa wartość życia herosa.\n" +
                "Moc - na jej podstawie ustalana będzie siła ataków herosa.\n");

            name = InputOutputClass.AskForData("Nazwa potwora");
            maxHealth = InputOutputClass.AskForIntFromTo("Żywotność", 1, 50);
            power = InputOutputClass.AskForIntFromTo("Moc", 1, 50);

            _monsterOperations.AddMonster(name, maxHealth * 7, power * 3);

            Console.WriteLine("Dodano nowego potwora! Naciśnij dowolny klawisz by przejść do menu.");
            Console.ReadKey();
        }

        public void StartNewGame(Hero hero)
        {
            Boolean playerDeath = false;
            Console.Clear();
            _playerOperations.SaveNewLineToTextFileInCurrentLocalization(_playerOperations.HeroStatus(hero),hero);

            do
            {
                Console.WriteLine(_playerOperations.HeroStatus(hero));
                if (new Random().Next(0, 100) > 50)
                {
                    int monsterId = new Random().Next(1, _monsterOperations.GetMonstersCount());
                    Console.WriteLine($"\nNapotkałeś potwora!\n{_monsterOperations.MonsterStatus(monsterId)}\n");
                    _playerOperations.SaveNewLineToTextFileInCurrentLocalization($"Napotkano potwora!\n{_monsterOperations.MonsterStatus(monsterId)}", hero);
                    if (InputOutputClass.AskForIntFromTo("1. Walcz!\n2.Uciekaj...\n\nDecyzja", 1, 2) == 1)
                    {
                        SingleFight fight = _fightOperations.Fight(hero, monsterId);
                        foreach (SingleHit hit in fight.HistoryOfAllHits)
                        {
                            Console.WriteLine($"{hit.HistoryOfHit}");
                            _playerOperations.SaveNewLineToTextFileInCurrentLocalization($"{hit.HistoryOfHit}", hero);
                            Console.ReadKey();
                        }
                        if (fight.Win)
                        {
                            Console.WriteLine("\nPo zaciekłej bójce Herosowi udało się zabić potwora!");
                            _playerOperations.SaveNewLineToTextFileInCurrentLocalization($"Po zaciekłej bójce Herosowi udało się zabić potwora!", hero);
                        }
                        else
                        {
                            Console.WriteLine("\nNiestety potwór okazał się zbyt potężny i pokonał Herosa.");
                            _playerOperations.SaveNewLineToTextFileInCurrentLocalization($"Niestety potwór okazał się zbyt potężny i pokonał Herosa.", hero);
                        }
                        Console.ReadKey();
                    }
                    else
                    {
                        string runAwayStatus = _fightOperations.RunAway(hero, monsterId);
                        Console.WriteLine(runAwayStatus);
                        _playerOperations.SaveNewLineToTextFileInCurrentLocalization(runAwayStatus, hero);
                    }
                }
                else
                {
                    string incidentStatus = _incidentsOperations.NeutralIncident(hero, new Random().Next(1, _incidentsOperations.GetIncidentsCount()));
                    Console.WriteLine(incidentStatus);
                    _playerOperations.SaveNewLineToTextFileInCurrentLocalization(incidentStatus, hero);
                    Console.ReadKey();
                }
                if (_playerOperations.AlreadyDied(hero))
                {
                    _playerOperations.SavePathToHistoryFile(hero);
                    Console.WriteLine("To już koniec zmagań Herosa. Żegnaj na zawsze!\n" + _playerOperations.HeroLastStatus(hero));
                    _playerOperations.SaveNewLineToTextFileInCurrentLocalization("To już koniec zmagań Herosa. Żegnaj na zawsze!\n" + _playerOperations.HeroLastStatus(hero), hero);
                    Console.ReadKey();
                    playerDeath = true;
                }
            } while (!playerDeath);
        }

                public void ShowScoreBoard()
        {
            Console.Clear();
            int index = 0;
            Console.WriteLine("Tablica wyników!\n\n");
            foreach (string heroStatus in _playerOperations.ChoosePlayerFromAlreadyDiedOrderedByPoints())
            {
                index++;
                Console.WriteLine($"{index}. {heroStatus}");
                if (index == 10)
                {
                    break;
                }
            }
            if (index == 0)
            {
                Console.WriteLine("Brak Wwyników!");
            }
            Console.WriteLine("\nNaciśnij dowolny klawisz, żeby powrócić do MENU.");
            Console.ReadKey();
        }

        public void ShowGameHistory()
        {
            Console.Clear();
            int index = 0;
            Console.WriteLine("Tablica wyników!\n\n");
            foreach (string heroStatus in _playerOperations.ChoosePlayerFromAlreadyDied())
            {
                index++;
                Console.WriteLine($"{index}. {heroStatus}");
            }
            if (index == 0)
            {
                Console.WriteLine("Brak Wyników!");
            }
            else
            {
                index = InputOutputClass.AskForIntFromTo("\nWyświetl historię Herosa numer", 1, index);
                Console.Clear();
                using (StreamReader history = new StreamReader($"{_playerOperations.GetPathToHistoryFile(index)}"))
                {
                    string gameHistory = history.ReadToEnd();
                    Console.WriteLine(gameHistory);
                }
            }

            Console.WriteLine("\n\nNaciśnij dowolny klawisz, żeby powrócić do MENU!");
            Console.ReadKey();
        }
    }
}
