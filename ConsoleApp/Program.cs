﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp;
using GameLogic;
using GameLogic.Interfaces;
using GameLogic.Fight;
using ConsoleApp.Interfaces;
using Ninject;
using DataAccess.Entities;

namespace ConsoleUserInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new DataAccessLayerModule());
            kernel.Bind<IEquipmentOperations>().To<EquipmentOperations>();
            kernel.Bind<IFightsOperations>().To<FightsOperations>();
            kernel.Bind<IIncidentsOperations>().To<IncidentsOperations>();
            kernel.Bind<IMonsterOperations>().To<MonsterOperations>();
            kernel.Bind<IPlayerOperations>().To<PlayerOperations>();
            kernel.Bind<IInitialOperations>().To<InitialOperations>();
            kernel.Bind<IMenuOptions>().To<MenuOptions>();
            
            PlayerOperations heroOperations = kernel.Get<PlayerOperations>();
            MenuOptions menuOptions = kernel.Get<MenuOptions>();
            InitialOperations initialOperations = kernel.Get<InitialOperations>();

            initialOperations.AddMissinginitialData();
            Hero hero = kernel.Get<Hero>();

            Boolean exit = false;

            do
            {
                switch (InputOutputClass.menuBar())
                {
                    case 1:
                        hero = kernel.Get<Hero>();
                        hero = menuOptions.CreateNewPlayer();
                        break;
                    case 2:
                        menuOptions.CreateNewMonster();
                        break;
                    case 3:
                        if (hero.CurrentHealth > 0 && hero.Id != 0)
                        {
                                menuOptions.StartNewGame(hero);
                        }
                        else
                        {
                            Console.WriteLine("\n\nMusisz stworzyć nową postać!");
                            Console.ReadKey();
                        }
                        break;
                    case 4:
                        menuOptions.ShowScoreBoard();
                        break;
                    case 5:
                        menuOptions.ShowGameHistory();
                        break;
                    case 6:
                        Console.Clear();
                        Console.WriteLine("\nŻegnaj!");
                        exit = true;
                        break;
                    default:
                        break;
                }

            } while (!exit);
        }
    }
}
