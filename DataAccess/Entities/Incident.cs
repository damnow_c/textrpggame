﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class Incident
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public int KindOfEffect { get; set; }
    }
}
