﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public string SortOfItem { get; set; }
        public string TypeOfModificator { get; set; }
    }
}
