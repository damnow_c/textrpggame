﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class Hero
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MaxHealth { get; set; }
        public int CurrentHealth { get; set; }
        public int Power { get; set; }
        public int Luck { get; set; }
        public int Agility { get; set; }
        public int Points { get; set; }
        public int WeaponId { get; set; }
        public virtual Weapon Weapon { get; set; }
        public int ArmorId { get; set; }
        public virtual Armor Armor { get; set; }
        public string ItemsNames { get; set; }
        public string PathToHistoryFile { get; set; }
        public DateTime EndGameDate { get; set; }
    }
}
