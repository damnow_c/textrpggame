﻿using System;
using System.Data.Entity;
using DataAccess.Entities;

namespace DataAccess
{
    public class GameDbContext : DbContext
    {
        public GameDbContext() : base("GameDbConnectionString")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<GameDbContext>());
            //Database.SetInitializer(new CreateDatabaseIfNotExists<GameDbContext>());
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<GameDbContext, Migrations.Configuration>());
        }

        public DbSet<Hero> Heros { get; set; }

        public DbSet<Monster> Monsters { get; set; }

        public DbSet<Incident> Incidents { get; set; }

        public DbSet<Armor> Armors { get; set; }

        public DbSet<Weapon> Weapons { get; set; }

        public DbSet<Item> Items { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(x => x.HasColumnType("datetime2"));
        }
    }
}
