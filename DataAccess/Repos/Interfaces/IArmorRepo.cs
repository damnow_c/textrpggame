﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IArmorRepo
    {
        void Create(Armor armor);
        IEnumerable<Armor> GetAll();
        Armor GetById(int id);
        int GetNumberOfArmors();
    }
}