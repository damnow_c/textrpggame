﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IIncidentRepo
    {
        void Create(Incident incident);
        IEnumerable<Incident> GetAll();
        Incident GetById(int id);
    }
}