﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IHeroRepo
    {
        void Create(Hero hero);
        IEnumerable<Hero> GetAll();
        Hero GetById(int id);
        void Update(Hero hero);
    }
}