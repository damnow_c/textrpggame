﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IMonsterRepo
    {
        void Create(Monster monster);
        IEnumerable<Monster> GetAll();
        Monster GetById(int id);
        int GetNumberOfMonsters();
    }
}