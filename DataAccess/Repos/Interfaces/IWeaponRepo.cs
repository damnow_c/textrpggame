﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IWeaponRepo
    {
        void Create(Weapon weapon);
        IEnumerable<Weapon> GetAll();
        Weapon GetById(int id);
        int GetNumberOfWeapons();
    }
}