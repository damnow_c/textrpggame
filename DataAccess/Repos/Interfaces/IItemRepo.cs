﻿using System.Collections.Generic;
using DataAccess.Entities;

namespace DataAccess.Repos.Interfaces
{
    public interface IItemRepo
    {
        void Create(Item item);
        IEnumerable<Item> GetAll();
        Item GetById(int id);
        int GetNumberOfItems();
    }
}