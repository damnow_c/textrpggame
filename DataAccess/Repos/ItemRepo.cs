﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class ItemRepo : IItemRepo
    {
        private GameDbContext _dbContext;
        public ItemRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Item item)
        {
            _dbContext.Items.Add(item);
            _dbContext.SaveChanges();
        }

        public Item GetById(int id)
        {
            Item item = _dbContext.Items.Find(id);
            return item;
        }

        public IEnumerable<Item> GetAll()
        {
            IEnumerable<Item> items = _dbContext.Items;
            return items;
        }

        public int GetNumberOfItems()
        {
            int numberOfItems = _dbContext.Items.ToList().Count;
            return numberOfItems;
        }
    }
}