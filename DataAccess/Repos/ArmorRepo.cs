﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class ArmorRepo : IArmorRepo
    {
        private GameDbContext _dbContext;
        public ArmorRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Armor armor)
        {
            _dbContext.Armors.Add(armor);
            _dbContext.SaveChanges();
        }

        public Armor GetById(int id)
        {
            Armor armor = _dbContext.Armors.Find(id);
            return armor;
        }

        public IEnumerable<Armor> GetAll()
        {
            IEnumerable<Armor> armors = _dbContext.Armors;
            return armors;
        }

        public int GetNumberOfArmors()
        {
            int numberOfArmors = _dbContext.Armors.ToList().Count;
            return numberOfArmors;
        }
    }
}
