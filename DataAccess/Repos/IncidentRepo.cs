﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class IncidentRepo : IIncidentRepo
    {
        private GameDbContext _dbContext;
        public IncidentRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Incident incident)
        {
            _dbContext.Incidents.Add(incident);
            _dbContext.SaveChanges();
        }

        public Incident GetById(int id)
        {
            Incident incident = _dbContext.Incidents.Find(id);
            return incident;
        }

        public IEnumerable<Incident> GetAll()
        {
            IEnumerable<Incident> incidents = _dbContext.Incidents;
            return incidents;
        }

    }
}
