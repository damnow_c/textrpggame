﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using Moq;

namespace DataAccess.Repos
{
    public class WeaponRepo : IWeaponRepo
    {
        private GameDbContext _dbContext;
        public WeaponRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Weapon weapon)
        {
            _dbContext.Weapons.Add(weapon);
            _dbContext.SaveChanges();
        }

        public Weapon GetById(int id)
        {
            Weapon weapon = _dbContext.Weapons.Find(id);
            return weapon;
        }

        public IEnumerable<Weapon> GetAll()
        {
            IEnumerable<Weapon> weapons = _dbContext.Weapons;
            return weapons;
        }

        public int GetNumberOfWeapons()
        {
            int numberOfWeapons = _dbContext.Weapons.ToList().Count;
            return numberOfWeapons;
        }

        public static implicit operator WeaponRepo(Mock<IWeaponRepo> v)
        {
            throw new NotImplementedException();
        }
    }
}
