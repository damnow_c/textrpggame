﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;
using Moq;

namespace DataAccess.Repos
{
    public class HeroRepo : IHeroRepo
    {
        private GameDbContext _dbContext;
        public HeroRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Hero hero)
        {
            _dbContext.Heros.Add(hero);
            _dbContext.SaveChanges();
        }

        public Hero GetById(int id)
        {
            Hero hero = _dbContext.Heros.Find(id);
            return hero;
        }

        public IEnumerable<Hero> GetAll()
        {
            IEnumerable<Hero> heros = _dbContext.Heros;
            return heros;
        }

        public void Update(Hero hero)
        {
            Hero updatedHero = _dbContext.Heros.Find(hero.Id);
            updatedHero = hero;
            _dbContext.SaveChanges();
        }

        public static implicit operator HeroRepo(Mock<IWeaponRepo> v)
        {
            throw new NotImplementedException();
        }

        public static implicit operator HeroRepo(Mock<IHeroRepo> v)
        {
            throw new NotImplementedException();
        }
    }
}
