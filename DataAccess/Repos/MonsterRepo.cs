﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataAccess.Entities;
using DataAccess.Repos.Interfaces;

namespace DataAccess.Repos
{
    public class MonsterRepo : IMonsterRepo
    {
        private GameDbContext _dbContext;
        public MonsterRepo(GameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Monster monster)
        {
            _dbContext.Monsters.Add(monster);
            _dbContext.SaveChanges();
        }

        public Monster GetById(int id)
        {
            Monster monster = _dbContext.Monsters.Find(id);
            return monster;
        }

        public IEnumerable<Monster> GetAll()
        {
            IEnumerable<Monster> monsters = _dbContext.Monsters;
            return monsters;
        }

        public int GetNumberOfMonsters()
        {
            int numberOfMonsters = _dbContext.Monsters.ToList().Count ;
            return numberOfMonsters;
        }
    }
}
